//
//  WeatherClientMock.swift
//  bring-ios-exerciseTests
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation
@testable import bring_ios_exercise
import PromiseKit
class WeatherClientMock: WeatherClientProtocol {
    func weather(forCoordinates coords: (lat: Double, lon: Double)) -> Promise<CurrentWeather> {
        return  Promise<CurrentWeather> { seal in
            
        }
    }
    
    func weather(forIds ids: [String]) -> Promise<[CurrentWeather]> {
        return  Promise<[CurrentWeather]> { seal in
            seal.fulfill([
                CurrentWeather(id: 1, name: "Testing", timezone: nil, coord: Coord(lat: 123.123, lon: 123.123), weather: [Weather(id: 1, main: "clear", desc: "Clear Sky")], main: Main(temp: 12, feelsLike: 13, tempMin: 1, tempMax: 15, pressure: 12, humidity: 60), visibility: 12, pop: nil, wind: Wind(speed: 120, deg: 180)),
                CurrentWeather(id: 2, name: "Testing2", timezone: nil, coord: Coord(lat: 123.123, lon: 123.123), weather: [Weather(id: 1, main: "clear", desc: "Clear Sky")], main: Main(temp: 12, feelsLike: 13, tempMin: 1, tempMax: 15, pressure: 12, humidity: 60), visibility: 12, pop: nil, wind: Wind(speed: 120, deg: 180)),
                CurrentWeather(id: 3, name: "Testing3", timezone: nil, coord: Coord(lat: 123.123, lon: 123.123), weather: [Weather(id: 1, main: "clear", desc: "Clear Sky")], main: Main(temp: 12, feelsLike: 13, tempMin: 1, tempMax: 15, pressure: 12, humidity: 60), visibility: 12, pop: nil, wind: Wind(speed: 150, deg: 180))
            ])
        }
    }
    
    
}
