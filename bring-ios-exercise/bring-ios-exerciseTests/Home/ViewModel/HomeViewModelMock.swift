//
//  HomeViewModelMock.swift
//  bring-ios-exerciseTests
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation
@testable import bring_ios_exercise
import PromiseKit
class HomeViewModelMock: HomeViewModelProtocol {
    var canAddMore: Bool = false
    
    var list = [CurrentWeather]()
    
    required init(cityClient: CityClientProtocol, weatherClient: WeatherClientProtocol) {
        
    }
    
    func load() -> Promise<[Item]> {
        return Promise<[Item]> { seal in
            seal.fulfill([
                Item(name: "Testing", imageName: "clear", temp: "17.0º")
            ])
        }
    }
    
    func delete(atIndex index: Int) {
        
    }
    
    
}
