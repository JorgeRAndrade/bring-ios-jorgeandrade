//
//  HomeTableViewControllerTests.swift
//  bring-ios-exerciseTests
//
//  Created by Jorge Andrade on 25/01/2022.
//

import XCTest
@testable import bring_ios_exercise

class HomeTableViewControllerTests: XCTestCase {
    var sut: HomeTableViewController!
    
    override func setUp() {
        super.setUp()
        
        let viewModel = HomeViewModelMock(cityClient: CityClientMock(), weatherClient: WeatherClientMock())
        
        sut = HomeTableViewController(viewModel: viewModel)
        
        self.sut.loadView()
        self.sut.viewDidLoad()
        
        sut.viewWillAppear(true)
        sut.viewDidAppear(true)
        sut.loadData()
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testHasATableView() {
        XCTAssertNotNil(sut.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(sut.tableView.delegate)
    }
    func testTableViewConfromsToTableViewDelegateProtocol() {
            XCTAssertTrue(sut.conforms(to: UITableViewDelegate.self))
            XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:didSelectRowAt:))))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(sut.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
            XCTAssertTrue(sut.conforms(to: UITableViewDataSource.self))
            XCTAssertTrue(sut.responds(to: #selector(sut.numberOfSections(in:))))
            XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:numberOfRowsInSection:))))
            XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:cellForRowAt:))))
        }
    func testTableViewCellHasReuseIdentifier() {
            let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? CityTableViewCell
            let actualReuseIdentifer = cell?.reuseIdentifier
            let expectedReuseIdentifier = "SideMenuTableViewCell"
            XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
        }
    func testTableData() throws {
        sleep(10)
        let numberOfRows = sut.tableView(sut.tableView, numberOfRowsInSection: 0)
        
        print("************************\n\(numberOfRows)\n*************************")
        guard let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? CityTableViewCell else {
            XCTFail()
            return
        }
        XCTAssertEqual(cell.nameLabel.text, "Testing")
        XCTAssertEqual(cell.temperatureLabel.text, "17º")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
