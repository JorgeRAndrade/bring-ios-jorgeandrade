//
//  HomeTests.swift
//  bring-ios-exerciseTests
//
//  Created by Jorge Andrade on 25/01/2022.
//

import XCTest
@testable import bring_ios_exercise

class HomeViewModelTests: XCTestCase {
    var sut: HomeViewModel!
    override func setUp() {
        super.setUp()
        sut = HomeViewModel(cityClient: CityClientMock(), weatherClient: WeatherClientMock())
    }
    func testModelParsing() throws {
        
        
        sut.load().done { items in
            
            XCTAssertEqual(items.first?.name, "Testing")
            XCTAssertEqual(items.first?.imageName, "clear")
            XCTAssertEqual(items.first?.temp, "12.0º")
        }.catch { error in
            XCTFail()
        }
        
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
