//
//  CityClientMock.swift
//  bring-ios-exerciseTests
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation
@testable import bring_ios_exercise

class CityClientMock: CityClientProtocol {
    var cities: [City] = {
      var array = [
        City(name: "Testing", coord: Coord(lat: 1232.23, lon: 123.31), id: 1),
        City(name: "Testing2", coord: Coord(lat: 1232.23, lon: 123.31), id: 2)
      ]
        
        return array
    }()
    
    func delete(atIndex index: Int) {
        
    }
    
    func save(_ city: City) {
        
    }
    
    
}
