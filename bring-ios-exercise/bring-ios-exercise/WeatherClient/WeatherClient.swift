//
//  WeatherClient.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 24/01/2022.
//

import Foundation
import PromiseKit

struct WeatherClient: WeatherClientProtocol {
    func weather(forIds ids: [String]) -> Promise<[CurrentWeather]> {
        
        return Promise<[CurrentWeather]> { seal in
            let session = URLSession(configuration: .default)
            
            let queryItems = [
                URLQueryItem(name: "id", value: ids.joined(separator: ",")),
                URLQueryItem(name: "units", value: "metric"),
                URLQueryItem(name: "appid", value: Config.Api.apiKey),
            ]
            
            guard var urlComps = URLComponents(string: Config.Api.baseUrl + Config.Api.EndPoint.group) else {
                
                let error = NSError(domain: "", code: 400, userInfo: ["localized": "Invalid URL"])
                seal.reject(error)
                return
            }
            
            urlComps.queryItems = queryItems
            
            guard let url = urlComps.url else {
                let error = NSError(domain: "", code: 400, userInfo: ["localized": "Invalid URL"])
                seal.reject(error)
                return
            }
            
            print(url)
            
            let dataTask = session.dataTask(with: url) { (data, response, error) in
                
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                if let data = data, let weather = try? decoder.decode(WeatherList.self, from: data) {
                    seal.fulfill(weather.list)
                } else {
                    let error = NSError(domain: "", code: 400, userInfo: ["localized": "Invalid Data"])
                    seal.reject(error)
                }
                
            }
            dataTask.resume()
        }
        
    }
    
    func weather(forCoordinates coords: (lat: Double, lon: Double)) -> Promise<CurrentWeather>  {
        
        return Promise<CurrentWeather> { seal in
            let session = URLSession(configuration: .default)
            
            let queryItems = [
                URLQueryItem(name: "lat", value: String(coords.lat)),
                URLQueryItem(name: "lon", value: String(coords.lon)),
                URLQueryItem(name: "units", value: "metric"),
                URLQueryItem(name: "appid", value: Config.Api.apiKey),
            ]
            
            guard var urlComps = URLComponents(string: Config.Api.baseUrl + Config.Api.EndPoint.current) else {
                let error = NSError(domain: "", code: 400, userInfo: ["localized": "Invalid URL"])
                seal.reject(error)
                return
            }
            
            urlComps.queryItems = queryItems
            
            guard let url = urlComps.url else {
                let error = NSError(domain: "", code: 400, userInfo: ["localized": "Invalid URL"])
                seal.reject(error)
                return
            }
            
            print(url)
            
            let dataTask = session.dataTask(with: url) { (data, response, error) in
                
                
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                if let data = data, let weather = try? decoder.decode(CurrentWeather.self, from: data) {
                    seal.fulfill(weather)
                } else {
                    let error = NSError(domain: "", code: 400, userInfo: ["localized": "Invalid Data"])
                    seal.reject(error)
                }
                
            }
            dataTask.resume()
        }
        
    }
        
}

private struct WeatherList: Decodable{
    var list: [CurrentWeather]
}

protocol WeatherClientProtocol {
    func weather(forCoordinates coords: (lat: Double, lon: Double)) -> Promise<CurrentWeather>
    func weather(forIds ids: [String]) -> Promise<[CurrentWeather]>
}
