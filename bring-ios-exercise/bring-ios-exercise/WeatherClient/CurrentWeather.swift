//
//  CurrentWeather.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 24/01/2022.
//

import Foundation

struct CurrentWeather: Decodable {
    let id: Int
    let name: String
    let timezone: Int?
    let coord: Coord
    let weather: [Weather]
    let main: Main
    let visibility: Int
    let pop: Int?
    let wind: Wind
}



struct Coord: Codable {
    let lat: Double
    let lon: Double
}

struct Weather: Decodable {
    let id: Int
    let main: String
    let desc: String
    
    enum CodingKeys: String, CodingKey {
            case id, main, desc = "description"
        }
}

struct Main: Decodable {
    let temp: Float
    let feelsLike: Float
    let tempMin: Float
    let tempMax: Float
    let pressure: Int
    let humidity: Int
}

struct Wind: Decodable {
    let speed: Float
    let deg: Int
}
