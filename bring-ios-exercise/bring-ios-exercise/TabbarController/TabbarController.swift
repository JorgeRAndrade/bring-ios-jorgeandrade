//
//  TabbarController.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import UIKit

class TabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.isTranslucent = false
        var vc = Home.build()
        vc.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "contorno-da-casa"), tag: 0)
        
        let nav = UINavigationController()
        
        nav.viewControllers = [vc]
        self.viewControllers = [nav]
        self.selectedIndex = 0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
