//
//  Config.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 7/22/1399 AP.
//

import Foundation

struct Config {
    struct Api {
        static let baseUrl = "http://api.openweathermap.org/data/2.5/"
        static let apiKey = "c6e381d8c7ff98f0fee43775817cf6ad"
        
        struct EndPoint {
            static let current = "weather"
            static let forecast = "forecast"
            static let group = "group"
        }
    }
    
    struct UserDefaults {
        static let cityData = "BringIOSExercise.CityData"
    }
}
