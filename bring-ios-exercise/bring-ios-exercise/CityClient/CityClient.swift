//
//  CityClient.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 24/01/2022.
//

import Foundation
import MapKit

class CityClient: CityClientProtocol {
    let userDefaults = UserDefaults.standard
    
    var cities: [City] {
        get {
            guard let cityData = userDefaults.data(forKey: Config.UserDefaults.cityData), let array = try? JSONDecoder().decode([City].self, from: cityData) else {
                return []
            }
            
            return array
        }
        
        set {
            if let data = try? JSONEncoder().encode(newValue) {
                UserDefaults.standard.set(data, forKey: Config.UserDefaults.cityData)
            }
        }
    }
    
    func delete(atIndex index: Int) {
        var cities = cities
        cities.remove(at: index)
        self.cities = cities
    }
    
    func save(_ city: City) {
        var cities = cities
        cities.append(city)
        self.cities = cities
    }
}


protocol CityClientProtocol {
    var cities: [City] { get set }
    func delete(atIndex index: Int)
    func save(_ city: City)
}
