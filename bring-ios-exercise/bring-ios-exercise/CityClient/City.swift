//
//  City.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 7/22/1399 AP.
//

import Foundation
import MapKit
struct City: Codable {
    var name: String
    var coord: Coord
    var id: Int
    
    init(fromWeather weather: CurrentWeather) {
        self.name = weather.name
        self.coord = weather.coord
        self.id = weather.id
    }
    
    init(name: String, coord: Coord, id: Int) {
        self.name = name
        self.coord = coord
        self.id = id
    }
    
    func toAnnotation() -> MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: self.coord.lat, longitude: self.coord.lon)
        annotation.title = self.name
        return annotation
    }
}
