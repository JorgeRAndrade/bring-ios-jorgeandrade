//
//  Map.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation

struct Map {
    static func build(delegate: MapViewDelegate) -> MapViewController {
        let cityClient = CityClient()
        let weatherClient = WeatherClient()
        
        let viewModel = MapViewModel(cityClient: cityClient, weatherClient: weatherClient)
        
        return MapViewController(viewModel: viewModel, delegate: delegate)
    }
}
