//
//  MapViewModel.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 24/01/2022.
//

import Foundation
import MapKit
import PromiseKit
class MapViewModel: MapViewModelProtocol {
    private let weatherClient: WeatherClientProtocol
    private var cityClient: CityClientProtocol
    
    var annotations: [MKPointAnnotation] {
        return cityClient.cities.map { city in
            city.toAnnotation()
        }
    }
    required init(cityClient: CityClientProtocol, weatherClient:WeatherClientProtocol) {
        self.weatherClient = weatherClient
        self.cityClient = cityClient
    }
    
    func addCity(withCoords coords: CLLocationCoordinate2D) -> Promise<MKPointAnnotation> {
        return Promise<MKPointAnnotation> {seal in
            guard cityClient.cities.count < 20 else {
                seal.reject(MapError.limit)
                return
            }
            
            weatherClient.weather(forCoordinates: (lat: coords.latitude, lon: coords.longitude))
                .done { [weak self] weather in
                    guard let self = self else { return }
                    guard !self.cityClient.cities.map({ $0.id }).contains(weather.id) else {
                        seal.reject(MapError.duplicated(weather.name))
                        return
                    }
                    let city = City(fromWeather: weather)
                    self.cityClient.save(city)
                    seal.fulfill(city.toAnnotation())
                }.catch { error in
                    seal.reject(error)
                    return
                }
        }
    }
    
    enum MapError: Error {
        case duplicated(String)
        case limit
    }
}

protocol MapViewModelProtocol {
    var annotations: [MKPointAnnotation] { get }
    
    init(cityClient: CityClientProtocol, weatherClient:WeatherClientProtocol)
    func addCity(withCoords coords: CLLocationCoordinate2D) -> Promise<MKPointAnnotation>
}
