//
//  MapViewController+Extensions.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation
import UIKit

extension MapViewController {
    func setupView() {
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("cancel", comment: ""), style: .plain, target: self, action: #selector(dismissViewController))
        
        [mapView, loader].forEach {view.addSubview($0)}
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: self.view.topAnchor),
            mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            
            loader.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            loader.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            
        ])
    }
    
    func setupGestureRecognizer() {
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleTap))
        
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
        mapView.addAnnotations(viewModel.annotations)
    }
}
