//
//  MapViewController.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 7/22/1399 AP.
//

import UIKit
import MapKit

protocol MapViewDelegate: AnyObject {
    func onNewLocationAdded()
}

class MapViewController: UIViewController, UIGestureRecognizerDelegate  {
    
    
    let mapView: MKMapView = {
        let map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        
        return map
    }()
    
    let loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    var viewModel: MapViewModelProtocol
    
    
    weak var delegate: MapViewDelegate?
    
    init(viewModel: MapViewModelProtocol, delegate: MapViewDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleTap))
                
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
        
    }
    
    @objc func dismissViewController() {
        self.dismiss(animated: true)
    }
    
    @objc func handleTap(gestureRecognizer: UILongPressGestureRecognizer) {
        
        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        
        loader.isHidden = false
        loader.startAnimating()
        
        viewModel.addCity(withCoords: coordinate)
            .done { [weak self] annotation in
                guard let self = self else { return }
                
                self.loader.isHidden = true
                self.loader.stopAnimating()
                
                self.showAddedToListAlert(cityName: annotation.title ?? "")
                self.mapView.addAnnotation(annotation)
                self.delegate?.onNewLocationAdded()
            }.catch { [weak self]  error in
                
                guard let self = self else { return }
                
                self.loader.isHidden = true
                self.loader.stopAnimating()
                
                switch error {
                case MapViewModel.MapError.limit:
                    self.showLimitAlert()
                    
                case MapViewModel.MapError.duplicated(let name):
                    self.showAlreadyInListErrorAlert(cityName: name)
                    
                default:
                    self.showError(error)
                }
            }
    }
    
    func showError(_ error: Error) {
        let message = error.localizedDescription
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    func showLimitAlert() {
        let message = NSLocalizedString("delete-locations", comment: "")
        let alert = UIAlertController(title: NSLocalizedString("cannot-add-more", comment: ""), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func showAlreadyInListErrorAlert(cityName: String) {
        let message = String(format:  NSLocalizedString("local-already-in-list-message", comment: ""), cityName)
        let alert = UIAlertController(title: NSLocalizedString("local-already-in-list-title", comment: ""), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func showAddedToListAlert(cityName: String) {
        let message = String(format:  NSLocalizedString("local-added-to-list-message", comment: ""), cityName)
        let alert = UIAlertController(title: NSLocalizedString("local-added-to-list-title", comment: ""), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
}
