//
//  DetailViewModel.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import UIKit

class DetailViewModel: DetailViewModelProtocol {
    var weather: CurrentWeather

    var detail: DetailItem {
        return DetailItem(weather: self.weather)
    }
    
    required init(weather: CurrentWeather) {
        self.weather = weather
    }
}

struct DetailItem {
    let name: String
    let desc: String
    let imageName: String
    let temp: String
    let humidity: String
    let rainChance: String
    let windSpeed: String
    let windDirection: CGFloat
    
    private let degreesToRadians: (CGFloat) -> CGFloat = {
        return $0 / 180.0 * CGFloat(Double.pi)
    }
    
    init(weather: CurrentWeather) {
        name = weather.name
        desc = weather.weather[0].desc.capitalized
        imageName = weather.weather[0].main.lowercased()
        temp = "\(weather.main.temp)º"
        humidity = "\(weather.main.humidity)%"
        rainChance = "\(weather.pop ?? 0)%"
        windSpeed = "\(weather.wind.speed)m/s"
        windDirection = degreesToRadians(CGFloat((weather.wind.deg) + 180))
    }
}

protocol DetailViewModelProtocol {
    var detail: DetailItem { get }
    
    init(weather: CurrentWeather)
}

