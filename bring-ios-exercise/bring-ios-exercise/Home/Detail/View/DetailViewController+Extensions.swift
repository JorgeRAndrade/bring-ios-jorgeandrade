//
//  DetailViewController+Extensions.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation
import UIKit

extension DetailViewController {
    var verticalStackView: UIStackView {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.alignment = .leading
        return stack
    }
    func setupView() {
        self.view.backgroundColor = .white
        let humidityStack = verticalStackView
        
        [humidityTitleLabel, humidityLabel].forEach { humidityStack.addArrangedSubview($0) }
        
        
        let rainStack = verticalStackView
        
        [rainChancesTitleLabel, rainChancesLabel].forEach { rainStack.addArrangedSubview($0) }
        
        [humidityStack, rainStack].forEach { horizontalStackView.addArrangedSubview($0) }
        
        [windLabel, windArrow].forEach { windHorizontalStackView.addArrangedSubview($0) }
        
        let windStack = verticalStackView
        
        [windTitleLabel, windHorizontalStackView].forEach { windStack.addArrangedSubview($0) }
        
        let tempStack = verticalStackView
        tempStack.alignment = .center
        
        [descriptionLabel, temperatureLabel].forEach{ tempStack.addArrangedSubview($0) }
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        effectView.contentView.addSubview(tempStack)
        
        [weatherImageView, effectView].forEach { view.addSubview($0)}
        
        [view, horizontalStackView, windStack].forEach { mainStackView.addArrangedSubview($0) }
        
        self.view.addSubview(mainStackView)
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: self.view.topAnchor),
            
            mainStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            
            tempStack.centerXAnchor.constraint(equalTo: effectView.contentView.centerXAnchor),
            tempStack.centerYAnchor.constraint(equalTo: effectView.contentView.centerYAnchor),
            
            view.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor),
            view.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 3/4),
            
            weatherImageView.topAnchor.constraint(equalTo: view.topAnchor),
            weatherImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            weatherImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            weatherImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            effectView.topAnchor.constraint(equalTo: view.topAnchor),
            effectView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            effectView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            effectView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            horizontalStackView.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor, constant: 16),
            horizontalStackView.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor, constant: -16),
            
            windHorizontalStackView.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor, constant: 16),
            windHorizontalStackView.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor, constant: -16),
            
        ])
    }
    
    func setupData() {
        let detail = viewModel.detail
        let image = UIImage(named: detail.imageName) ?? UIImage(named: "mist")
        title = detail.name
        weatherImageView.image = image
        
        temperatureLabel.text = detail.temp
        
        descriptionLabel.text = detail.desc
        
        humidityTitleLabel.text = NSLocalizedString("humidity", comment: "")
        
        humidityLabel.text = detail.humidity
        
        rainChancesTitleLabel.text = NSLocalizedString("pop", comment: "")
        rainChancesLabel.text = detail.rainChance
        windTitleLabel.text = NSLocalizedString("wind", comment: "")
        
        windLabel.text = detail.windSpeed
        
        windArrow.transform = windArrow.transform.rotated(by: detail.windDirection)
    }
}
