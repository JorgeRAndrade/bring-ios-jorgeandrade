//
//  Detail.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation

struct Detail {
    static func build(with weather: CurrentWeather) -> DetailViewController {
 
        let viewModel = DetailViewModel(weather: weather)
        
        return DetailViewController(viewModel: viewModel)
    }
}
