//
//  CityTableViewCell.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 7/22/1399 AP.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 22, weight: .semibold)
        label.textColor = .white
        return label
    }()
    
    let weatherImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    let temperatureLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 40)
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    
    func setupView() {
        [weatherImageView, nameLabel, temperatureLabel ].forEach { self.contentView.addSubview($0)}
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
            nameLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            
            weatherImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            weatherImageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            weatherImageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            weatherImageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            
            temperatureLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16),
            temperatureLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
            
        ])
    }
}
