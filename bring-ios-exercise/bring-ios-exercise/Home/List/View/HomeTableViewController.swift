//
//  HomeTableViewController.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 7/22/1399 AP.
//

import UIKit
import RxSwift

class HomeTableViewController: UITableViewController {
    
    var items = [Item]()
    var viewModel: HomeViewModelProtocol
    
    var disposeBag = DisposeBag()
    
    init(viewModel: HomeViewModelProtocol) {
        self.viewModel = viewModel
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = NSLocalizedString("title", comment: "")
        
        loadData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(CityTableViewCell.self, forCellReuseIdentifier: "city")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let button = UIBarButtonItem(title: NSLocalizedString("add-location", comment: ""), style: .plain, target: self, action: #selector(addLocation))
    
        self.navigationItem.rightBarButtonItem  = button
    }
    
    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "city", for: indexPath) as? CityTableViewCell else {
            return UITableViewCell()
        }

        cell.nameLabel.text = items[indexPath.row].name
        let image = UIImage(named: items[indexPath.row].imageName) ?? UIImage(named: "mist")
        cell.weatherImageView.image = image
        cell.temperatureLabel.text = items[indexPath.row].temp
        return cell
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            viewModel.delete(atIndex: indexPath.row)
            items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Detail.build(with: viewModel.list[indexPath.row])
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }

}

extension HomeTableViewController: MapViewDelegate {
    func onNewLocationAdded() {
       loadData()
    }
}
