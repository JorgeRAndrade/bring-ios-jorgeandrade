//
//  HomeTableViewController+Extensions.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 24/01/2022.
//

import UIKit
extension HomeTableViewController {
    func loadData() {
        viewModel.load()
            .done { [weak self] items in
                guard let self = self else { return }
                
                self.items = items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }.catch { error in
                switch error {
                case HomeViewModel.HomeError.empty:
                    self.showEmptyAlert()
                default:
                    self.showErrorAlert(error)
                }
            }
    }
    
    @objc func addLocation() {
        guard viewModel.canAddMore else {
            showCityLimitAlert()
            return
        }
        
        let nav = UINavigationController()
        
        let map = Map.build(delegate: self)
        
        nav.viewControllers = [map]
        
        self.present(nav, animated: true)
        
    }
    
    private func showEmptyAlert() {
        let message = NSLocalizedString("empty-list-message", comment: "")
        let alert = UIAlertController(title: NSLocalizedString("empty-list-title", comment: ""), message: message, preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("add-location", comment: ""), style: .default, handler: { (action) in
            self.addLocation()
            alert.dismiss(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("later", comment: ""), style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    private func showErrorAlert(_ error: Error) {
        let message = error.localizedDescription
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("retry", comment: ""), style: .default, handler: { (action) in
            self.viewModel.load()
            alert.dismiss(animated: true)
        }))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    private func showCityLimitAlert() {
        let message = NSLocalizedString("delete-locations", comment: "")
        let alert = UIAlertController(title: NSLocalizedString("cannot-add-more", comment: ""), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}
