//
//  HomeList.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 25/01/2022.
//

import Foundation

struct Home {
    static func build() -> HomeTableViewController {
        let cityClient = CityClient()
        let weatherClient = WeatherClient()
        
        let viewModel = HomeViewModel(cityClient: cityClient, weatherClient: weatherClient)
        
        return HomeTableViewController(viewModel: viewModel)
    }
}
