//
//  HomeViewModel.swift
//  bring-ios-exercise
//
//  Created by Jorge Andrade on 24/01/2022.
//

import Foundation
import UIKit
import RxRelay
import PromiseKit

class HomeViewModel: HomeViewModelProtocol {
    
    let cityClient: CityClientProtocol
    let weatherClient: WeatherClientProtocol
    var list = [CurrentWeather]()
    
    var canAddMore: Bool {
        cityClient.cities.count < 20
    }
    
    required init(cityClient: CityClientProtocol, weatherClient: WeatherClientProtocol) {
        self.cityClient = cityClient
        self.weatherClient = weatherClient
    }
    
    
    func load() -> Promise<[Item]> {
        
        return Promise<[Item]> { seal in
            let cities = cityClient.cities
            guard !cities.isEmpty else {
                seal.reject(HomeError.empty)
                return
            }
            
            let ids: [String] = cities.compactMap { (city) -> String in
                "\(city.id)"
            }
            
            weatherClient.weather(forIds: ids)
                .done { [weak self] list in
                    guard let self = self else {return}
                    self.list = list
                    let items = list.compactMap { Item(weather: $0) }
                    seal.fulfill(items)
                }.catch { [weak self] error in
                    guard let self = self else {return}
                    seal.reject(error)
                }
        }
        
    }
    
    func delete(atIndex index: Int) {
        cityClient.delete(atIndex: index)
    }
    
    
    
    
    enum HomeError: Error {
        case empty
    }
}

struct Item: Codable {
    var name: String
    var imageName: String
    var temp: String
    
    init(weather: CurrentWeather) {
        self.name = weather.name
        self.imageName = weather.weather[0].main.lowercased()
        self.temp = "\(weather.main.temp)º"
    }
    
    init(name: String, imageName: String, temp: String) {
        self.name = name
        self.imageName = imageName
        self.temp = temp
    }
}



protocol HomeViewModelProtocol {
    var canAddMore: Bool { get }
    var list: [CurrentWeather] { get set }
    
    init(cityClient: CityClientProtocol, weatherClient: WeatherClientProtocol)
    func load() -> Promise<[Item]>
    func delete(atIndex index: Int)
    
}
