# README #

### What is this repository for? ###
This repository is a 24h challenge done by Jorge Andrade

### How do I get set up? ###

* Clone the project
* Run the project in Xcode

### 20 locations limit ###
To show the list with updated values, the application calls the API to get a group of locations info. Unfortunately, this request is limited to 20 locations, and that's why the list has this limit

